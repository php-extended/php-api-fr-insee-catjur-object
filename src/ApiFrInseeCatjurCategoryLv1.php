<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-catjur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCatjur;

/**
 * ApiFrInseeCatjurCategoryLv1 class file.
 * 
 * This is a simple implementation of the ApiFrInseeCatjurCategoryLv1Interface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeCatjurCategoryLv1 implements ApiFrInseeCatjurCategoryLv1Interface
{
	
	/**
	 * The id of the category.
	 * 
	 * @var int
	 */
	protected int $_idLv1;
	
	/**
	 * The label of the category.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeCatjurCategoryLv1 with private members.
	 * 
	 * @param int $idLv1
	 * @param string $libelle
	 */
	public function __construct(int $idLv1, string $libelle)
	{
		$this->setIdLv1($idLv1);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the category.
	 * 
	 * @param int $idLv1
	 * @return ApiFrInseeCatjurCategoryLv1Interface
	 */
	public function setIdLv1(int $idLv1) : ApiFrInseeCatjurCategoryLv1Interface
	{
		$this->_idLv1 = $idLv1;
		
		return $this;
	}
	
	/**
	 * Gets the id of the category.
	 * 
	 * @return int
	 */
	public function getIdLv1() : int
	{
		return $this->_idLv1;
	}
	
	/**
	 * Sets the label of the category.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeCatjurCategoryLv1Interface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeCatjurCategoryLv1Interface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the label of the category.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
