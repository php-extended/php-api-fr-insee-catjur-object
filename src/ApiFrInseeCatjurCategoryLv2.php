<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-catjur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCatjur;

/**
 * ApiFrInseeCatjurCategoryLv2 class file.
 * 
 * This is a simple implementation of the ApiFrInseeCatjurCategoryLv2Interface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeCatjurCategoryLv2 implements ApiFrInseeCatjurCategoryLv2Interface
{
	
	/**
	 * The id of the category.
	 * 
	 * @var int
	 */
	protected int $_idLv2;
	
	/**
	 * The id of the related lv1.
	 * 
	 * @var int
	 */
	protected int $_idLv1;
	
	/**
	 * The label of the category.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeCatjurCategoryLv2 with private members.
	 * 
	 * @param int $idLv2
	 * @param int $idLv1
	 * @param string $libelle
	 */
	public function __construct(int $idLv2, int $idLv1, string $libelle)
	{
		$this->setIdLv2($idLv2);
		$this->setIdLv1($idLv1);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the category.
	 * 
	 * @param int $idLv2
	 * @return ApiFrInseeCatjurCategoryLv2Interface
	 */
	public function setIdLv2(int $idLv2) : ApiFrInseeCatjurCategoryLv2Interface
	{
		$this->_idLv2 = $idLv2;
		
		return $this;
	}
	
	/**
	 * Gets the id of the category.
	 * 
	 * @return int
	 */
	public function getIdLv2() : int
	{
		return $this->_idLv2;
	}
	
	/**
	 * Sets the id of the related lv1.
	 * 
	 * @param int $idLv1
	 * @return ApiFrInseeCatjurCategoryLv2Interface
	 */
	public function setIdLv1(int $idLv1) : ApiFrInseeCatjurCategoryLv2Interface
	{
		$this->_idLv1 = $idLv1;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related lv1.
	 * 
	 * @return int
	 */
	public function getIdLv1() : int
	{
		return $this->_idLv1;
	}
	
	/**
	 * Sets the label of the category.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeCatjurCategoryLv2Interface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeCatjurCategoryLv2Interface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the label of the category.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
