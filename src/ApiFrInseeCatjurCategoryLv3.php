<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-catjur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCatjur;

/**
 * ApiFrInseeCatjurCategoryLv3 class file.
 * 
 * This is a simple implementation of the ApiFrInseeCatjurCategoryLv3Interface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeCatjurCategoryLv3 implements ApiFrInseeCatjurCategoryLv3Interface
{
	
	/**
	 * The id of the category.
	 * 
	 * @var int
	 */
	protected int $_idLv3;
	
	/**
	 * The id of the related lv2.
	 * 
	 * @var int
	 */
	protected int $_idLv2;
	
	/**
	 * The year when this category was created.
	 * 
	 * @var int
	 */
	protected int $_createdYear;
	
	/**
	 * The year when this category was removed.
	 * 
	 * @var ?int
	 */
	protected ?int $_removedYear = null;
	
	/**
	 * The label of the category.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeCatjurCategoryLv3 with private members.
	 * 
	 * @param int $idLv3
	 * @param int $idLv2
	 * @param int $createdYear
	 * @param string $libelle
	 */
	public function __construct(int $idLv3, int $idLv2, int $createdYear, string $libelle)
	{
		$this->setIdLv3($idLv3);
		$this->setIdLv2($idLv2);
		$this->setCreatedYear($createdYear);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the category.
	 * 
	 * @param int $idLv3
	 * @return ApiFrInseeCatjurCategoryLv3Interface
	 */
	public function setIdLv3(int $idLv3) : ApiFrInseeCatjurCategoryLv3Interface
	{
		$this->_idLv3 = $idLv3;
		
		return $this;
	}
	
	/**
	 * Gets the id of the category.
	 * 
	 * @return int
	 */
	public function getIdLv3() : int
	{
		return $this->_idLv3;
	}
	
	/**
	 * Sets the id of the related lv2.
	 * 
	 * @param int $idLv2
	 * @return ApiFrInseeCatjurCategoryLv3Interface
	 */
	public function setIdLv2(int $idLv2) : ApiFrInseeCatjurCategoryLv3Interface
	{
		$this->_idLv2 = $idLv2;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related lv2.
	 * 
	 * @return int
	 */
	public function getIdLv2() : int
	{
		return $this->_idLv2;
	}
	
	/**
	 * Sets the year when this category was created.
	 * 
	 * @param int $createdYear
	 * @return ApiFrInseeCatjurCategoryLv3Interface
	 */
	public function setCreatedYear(int $createdYear) : ApiFrInseeCatjurCategoryLv3Interface
	{
		$this->_createdYear = $createdYear;
		
		return $this;
	}
	
	/**
	 * Gets the year when this category was created.
	 * 
	 * @return int
	 */
	public function getCreatedYear() : int
	{
		return $this->_createdYear;
	}
	
	/**
	 * Sets the year when this category was removed.
	 * 
	 * @param ?int $removedYear
	 * @return ApiFrInseeCatjurCategoryLv3Interface
	 */
	public function setRemovedYear(?int $removedYear) : ApiFrInseeCatjurCategoryLv3Interface
	{
		$this->_removedYear = $removedYear;
		
		return $this;
	}
	
	/**
	 * Gets the year when this category was removed.
	 * 
	 * @return ?int
	 */
	public function getRemovedYear() : ?int
	{
		return $this->_removedYear;
	}
	
	/**
	 * Sets the label of the category.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeCatjurCategoryLv3Interface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeCatjurCategoryLv3Interface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the label of the category.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
