<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-catjur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCatjur;

use Iterator;
use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;

/**
 * InseeJuridicCategoryEndpoint class file.
 * 
 * This class is a simple implementation of the InseeJuridicCategoryEndpointInterface.
 * 
 * @author Anastaszor
 */
class ApiFrInseeCatjurEndpoint implements ApiFrInseeCatjurEndpointInterface
{
	
	/**
	 * The reifier.
	 * 
	 * @var ?ReifierInterface
	 */
	protected ?ReifierInterface $_reifier = null;
	
	/**
	 * Builds a new endpoint with the given reifier.
	 * 
	 * @param ReifierInterface $reifier
	 */
	public function __construct(?ReifierInterface $reifier = null)
	{
		$this->_reifier = $reifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurEndpointInterface::getJuridicCategoryLv1Iterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getJuridicCategoryLv1Iterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/cat_jur_lv1.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeCatjurCategoryLv1::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurEndpointInterface::getJuridicCategoryLv2Iterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getJuridicCategoryLv2Iterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/cat_jur_lv2.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeCatjurCategoryLv2::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurEndpointInterface::getJuridicCategoryLv3Iterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getJuridicCategoryLv3Iterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/cat_jur_lv3.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeCatjurCategoryLv3::class, $iterator);
	}
	
	/**
	 * Gets the reifier.
	 * 
	 * @return ReifierInterface
	 */
	protected function getReifier() : ReifierInterface
	{
		if(null === $this->_reifier)
		{
			$this->_reifier = new Reifier();
		}
		
		return $this->_reifier;
	}
	
}
