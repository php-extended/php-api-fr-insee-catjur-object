<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-catjur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurEndpoint;
use PHPUnit\Framework\TestCase;

/**
 * InseeJuridicCategoryEndpointTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiFrInseeCatjurEndpointTest extends TestCase
{
	
	/**
	 * The endpoint to test.
	 * 
	 * @var ApiFrInseeCatjurEndpoint
	 */
	protected ApiFrInseeCatjurEndpoint $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testLv1() : void
	{
		foreach($this->_object->getJuridicCategoryLv1Iterator() as $lv1)
		{
			/** @var PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurCategoryLv1 $lv1 */
			$this->assertNotNull($lv1->getIdLv1());
			$this->assertNotEmpty($lv1->getLibelle());
		}
	}
	
	public function testLv2() : void
	{
		$lv1Ids = [];
		
		foreach($this->_object->getJuridicCategoryLv1Iterator() as $lv1)
		{
			/** @var PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurCategoryLv1 $lv1 */
			$lv1Ids[] = $lv1->getIdLv1();
		}
		
		foreach($this->_object->getJuridicCategoryLv2Iterator() as $lv2)
		{
			/** @var PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurCategoryLv2 $lv2 */
			$this->assertNotNull($lv2->getIdLv2());
			$this->assertNotNull($lv2->getIdLv1());
			$this->assertContains($lv2->getIdLv1(), $lv1Ids);
			$this->assertNotEmpty($lv2->getLibelle());
		}
	}
	
	public function testLv3() : void
	{
		$lv2Ids = [];
		
		foreach($this->_object->getJuridicCategoryLv2Iterator() as $lv2)
		{
			/** @var PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurCategoryLv2 $lv2 */
			$lv2Ids[] = $lv2->getIdLv2();
		}
		
		foreach($this->_object->getJuridicCategoryLv3Iterator() as $lv3)
		{
			/** @var PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurCategoryLv3 $lv3 */
			$this->assertNotNull($lv3->getIdLv3());
			$this->assertNotNull($lv3->getIdLv2());
			$this->assertContains($lv3->getIdLv2(), $lv2Ids);
			$this->assertNotEmpty($lv3->getLibelle());
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeCatjurEndpoint();
	}
	
}
