<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-catjur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCatjur\Test;

use PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurCategoryLv1;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeCatjurCategoryLv1Test test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurCategoryLv1
 * @internal
 * @small
 */
class ApiFrInseeCatjurCategoryLv1Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeCatjurCategoryLv1
	 */
	protected ApiFrInseeCatjurCategoryLv1 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIdLv1() : void
	{
		$this->assertEquals(12, $this->_object->getIdLv1());
		$this->_object->setIdLv1(25);
		$this->assertEquals(25, $this->_object->getIdLv1());
	}
	
	public function testGetLibelle() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibelle());
		$this->_object->setLibelle('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getLibelle());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeCatjurCategoryLv1(12, 'azertyuiop');
	}
	
}
