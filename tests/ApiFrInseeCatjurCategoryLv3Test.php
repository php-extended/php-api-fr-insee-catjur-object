<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-catjur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCatjur\Test;

use PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurCategoryLv3;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeCatjurCategoryLv3Test test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeCatjur\ApiFrInseeCatjurCategoryLv3
 * @internal
 * @small
 */
class ApiFrInseeCatjurCategoryLv3Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeCatjurCategoryLv3
	 */
	protected ApiFrInseeCatjurCategoryLv3 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIdLv3() : void
	{
		$this->assertEquals(12, $this->_object->getIdLv3());
		$this->_object->setIdLv3(25);
		$this->assertEquals(25, $this->_object->getIdLv3());
	}
	
	public function testGetIdLv2() : void
	{
		$this->assertEquals(12, $this->_object->getIdLv2());
		$this->_object->setIdLv2(25);
		$this->assertEquals(25, $this->_object->getIdLv2());
	}
	
	public function testGetCreatedYear() : void
	{
		$this->assertEquals(12, $this->_object->getCreatedYear());
		$this->_object->setCreatedYear(25);
		$this->assertEquals(25, $this->_object->getCreatedYear());
	}
	
	public function testGetRemovedYear() : void
	{
		$this->assertNull($this->_object->getRemovedYear());
		$this->_object->setRemovedYear(25);
		$this->assertEquals(25, $this->_object->getRemovedYear());
	}
	
	public function testGetLibelle() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibelle());
		$this->_object->setLibelle('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getLibelle());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeCatjurCategoryLv3(12, 12, 12, 'azertyuiop');
	}
	
}
