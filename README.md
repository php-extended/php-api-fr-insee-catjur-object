# php-extended/php-api-fr-insee-catjur-object

This library is an implementation of the php-extended/php-api-fr-insee-catjur-interface library.

A library that makes insee Nomenclature des Activités Françaises (catjur) available
as objects to populate a database.
History is available at the url : [https://www.insee.fr/fr/information/2028129](https://www.insee.fr/fr/information/2028129).

![coverage](https://gitlab.com/php-extended/php-api-fr-insee-catjur-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-fr-insee-catjur-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-fr-insee-catjur-object ^8`


## Basic Usage

You may use this library the following way :

```php

use PhpExtended\Insee\InseeJuridicCategoryEndpoint;

$endpoint   = new InseeJuridicCategoryEndpoint();
$n1 = $endpoint->getJuridicCategoryLv1Iterator(); // returns iterator of InseeJuridicCategoryLv1 objects
$n2 = $endpoint->getJuridicCategoryLv2Iterator(); // returns iterator of InseeJuridicCategoryLv2 objects
$n3 = $endpoint->getJuridicCategoryLv3Iterator(); // returns iterator of InseeJuridicCategoryLv3 objects

```


## License

- The code is under MIT (See [license file](LICENSE)).

- The data is under Open License ([English](https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf) / [Français](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf))
